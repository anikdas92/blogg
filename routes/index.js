const express = require('express');
const router = express.Router();

const postService = require("../services/post");

/* GET home page. */
router.get('/', async function(req, res, next) {
    
    const posts = await postService.getAllPosts();
    res.render('index', { posts, title: "Home" });
});


router.use('/my_posts',async function (req, res, next) {

    if(req.session.user) {
        next()
    }else{
        res.redirect("/");
    }
})
router.get('/my_posts', async function(req, res, next) {
    
    const posts = await postService.getPostsByUser(req.session.user);
    res.render('user_posts', { posts, title: "My posts" });
});

router.get('/signin', async function(req, res, next) {
    
    if (req.session.user) {
        res.redirect('/');
    }else{
        res.render('signin', { title: "Blogg | Sign in" });
    }
});

router.get('/signup', async function(req, res, next) {
    
    if (req.session.user) {
        res.redirect('/');
    }else{
        res.render('signup', { title: "Blogg | Sign up" });
    }
});

router.get('/logout', async function(req, res, next) {
    
    if (req.session.user) {
        req.session.user = null;
    }
    res.redirect('/');
});


router.use('/newpost',async function (req, res, next) {

    if(req.session.user) {
        next()
    }else{
        res.redirect("/");
    }
})
router.get('/newpost', async function(req, res, next) {
    
    res.render('new_post', { title: "New Post" });
    
});


module.exports = router;
