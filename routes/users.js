const express = require('express');
const router = express.Router();

const userService = require("../services/user");

router.post('/signup',async function (req, res, next) {
    const payload = req.body;
    
    let resp = {};
    try {
        const createdUser = await userService.createUser(payload);
        const user = {
            id: createdUser.id,
            name: createdUser.name,
            email: createdUser.email
        }
        req.session.user = user;
        resp.success = true;
        resp.message = "Signup success";
    } catch (error) {
        // TODO: Log error
        resp.success = false;
        resp.message = "sign up failed";
    }

    res.send(resp);
});

router.post('/signin',async function (req, res, next) {
    const payload = req.body;
    
    let resp = {};
    try {
        const user = await userService.signIn(payload);

        req.session.user = user;
        resp.success = true;
        resp.message = "Signin success";
        resp.data = user;
    } catch (error) {
        // TODO: Log error
        resp.success = false;
        resp.message = error.message ? error.message : "signin failed";
    }

    res.send(resp);
});

router.post('/signinAjax',async function (req, res, next) {
    const payload = req.body;
    
    let resp = {};
    try {
        const user = await userService.signIn(payload);

        req.session.user = user;
        resp.success = true;
        resp.message = "Signin success";
        resp.data = user;
    } catch (error) {
        // TODO: Log error
        resp.success = false;
        resp.message = "signin failed";
    }

    res.send(resp);
});

router.use('/posts',async function (req, res, next) {

    if(req.session.user) {
        next()
    }else{
        res.sendStatus(400);
    }
});

module.exports = router;
