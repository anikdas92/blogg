const express = require('express');
const router = express.Router();

const postService = require("../services/post");
const commentService = require("../services/comment");

router.use('/create',async function (req, res, next) {

    if(req.session.user) {
        next()
    }else{
        res.sendStatus(400);
    }
})
router.post('/create',async function (req, res, next) {
    const payload = req.body;
    
    let resp = {};
    try {
        const post = await postService.createPost(payload, req.session.user);
        resp.success = true;
        resp.message = "new post created";
        resp.data = post;
    } catch (error) {
        // TODO: Log error
        resp.success = false;
        resp.message = "post creation failed";
    }

    res.send(resp);
});

router.get('/:postId',async function (req, res, next) {

    const postId = req.params.postId;
    
    try {
        const post = await postService.getPost(postId);
        res.render("show_post", { post, title: post.title });
    } catch (error) {
        // TODO: Log error
        res.sendStatus(400);
    }
});

router.use('/:postId/add_comment',async function (req, res, next) {

    if(req.session.user) {
        next()
    }else{
        res.sendStatus(400);
    }
})
router.post('/:postId/add_comment',async function (req, res, next) {
    const payload = req.body;
    const postId = req.params.postId;
    
    let resp = {};
    
    try {
        const comment = await commentService.addComment(payload, postId, req.session.user);
        resp.success = true;
        resp.message = "new comment created";
        resp.data = { comment, user: req.session.user };
    } catch (error) {
        // TODO: Log error
        resp.success = false;
        resp.message = "commend add failed";
    }

    res.send(resp);
});

module.exports = router;
