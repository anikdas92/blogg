const { expect, assert } = require('chai');

const syncDb = require("./sync_db");

const userService = require("../services/user");

before(function () {
    return syncDb.then(() => {

        const payload = {
            name: "Anik Das",
            password: "123456",
            email: "test@testf1.com"
        }

        return userService.createUser(payload);
    });
})
describe("userService", function () {
    it("should create an user", async function () {
        const payload = {
            name: "Anik Das",
            password: "123456",
            email: "test@testf.com"
        }


        var threwError = false;
        try {
            await userService.createUser(payload);

        } catch (error) {
            console.log(error);
            threwError = true;
        } 

        expect(threwError).to.be.false;

    })

    it("should not create an user", async function () {
        const payload = {
            name: "Anik Das",
            password: "123456"
        }


        let threwError = false;
        try {
            await userService.createUser(payload);

        } catch (error) {
            console.log(error);
            threwError = true;
        } 

        expect(threwError).to.be.true;

    })

    it("should sign user in", async function () {

        const payload = {
            email: "test@testf1.com",
            password: "123456"
        }

        let threwError = false;
        try {
            await userService.signIn(payload);

        } catch (error) {
            console.log(error);
            threwError = true;
        } 

        expect(threwError).to.be.false;

    })

    it("should not sign user in", async function () {

        const payload = {
            email: "test@testf1.com",
            password: "1234567"
        }

        let threwError = false;
        try {
            await userService.signIn(payload);

        } catch (error) {
            console.log(error);
            threwError = true;
        } 

        expect(threwError).to.be.true;

    })
})