const { expect, assert } = require('chai');

const syncDb = require("./sync_db");

const postService = require("../services/post");
const userService = require("../services/user");

before(function () {
    let user = null;
    return syncDb.then(() => {

        const payload = {
            name: "Anik Das",
            password: "123456",
            email: "test@testf2.com"
        }

        return userService.createUser(payload);
    }).then((createdUser) => {
        
        user = createdUser;

        const payload = {
            title: "Post title",
            content: "test content"
        }

        return postService.createPost(payload, user);
    });
})
describe("postService", function () {
    it("should create a post", async function () {
        const payload = {
            title: "Post title",
            content: "test content"
        }


        let threwError = false;
        try {
            await postService.createPost(payload, {id : 1});

        } catch (error) {
            threwError = true;
        } 

        expect(threwError).to.be.false;

    })

    it("should get a post", async function () {

        let threwError = false;
        let post = null;
        try {
            post = await postService.getPost(1);

        } catch (error) {
            threwError = true;
        }

        expect(threwError).to.be.false;
        expect(post).to.be.not.null;

    })

    it("should return all posts", async function () {

        let threwError = false;
        let posts = [];
        try {
            post = await postService.getAllPosts();

        } catch (error) {
            threwError = true;
        }

        expect(threwError).to.be.false;
        expect(posts).to.be.not.equal([]);

    })

    it("should return all posts by user", async function () {

        let threwError = false;
        let posts = [];
        try {
            post = await postService.getPostsByUser({id: 1});

        } catch (error) {
            threwError = true;
        }

        expect(threwError).to.be.false;
        expect(posts).to.be.not.equal([]);

    })
})
