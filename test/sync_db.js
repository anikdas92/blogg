const { User } = require("../models/user");
const { Post } = require("../models/post");
const { Comment } = require("../models/comment");

Post.belongsTo(User);
Comment.belongsTo(User);
Comment.belongsTo(Post);
User.hasMany(Post);
Post.hasMany(Comment);
User.hasMany(Comment);



User.sync({
    force: true
})
.then(Post.sync({force: true}))
.then(Comment.sync({force: true}))
.error(err => {
    // TODO: Log
    throw err;
})

module.exports = User.sync({
    force: true
})
.then(Post.sync({force: true}))
.then(Comment.sync({force: true}))
.error(err => {
    // TODO: Log
    throw err;
})