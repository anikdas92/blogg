const { expect, assert } = require('chai');

const syncDb = require("./sync_db");

const postService = require("../services/post");
const userService = require("../services/user");
const commentService = require("../services/comment");

before(function () {
    let user = null;
    return syncDb.then(() => {

        const payload = {
            name: "Anik Das",
            password: "123456",
            email: "test@testf3.com"
        }

        return userService.createUser(payload);
    }).then((createdUser) => {
        
        user = createdUser;

        const payload = {
            title: "Post title",
            content: "test content"
        }

        return postService.createPost(payload, user);
    });
})
describe("commentService", function () {
    it("should create a comment", async function () {
        
        const payload = {
            content: "test content"
        }

        let threwError = false;
        try {
            await commentService.addComment(payload, 1, { id: 1 });

        } catch (error) {
            threwError = true;
        } 

        expect(threwError).to.be.false;

    })

    it("should not create a comment", async function () {

        const payload = {
        }

        let threwError = false;
        try {
            await commentService.addComment(payload, 1, { id: 1 });

        } catch (error) {
            threwError = true;
        } 

        expect(threwError).to.be.true;

    })

    it("should not create a comment for having wrong user", async function () {

        const payload = {
            content: "test content"
        }

        let threwError = false;
        try {
            await commentService.addComment(payload, 1, { id: 200 });

        } catch (error) {
            threwError = true;
        } 

        expect(threwError).to.be.true;

    })
})
