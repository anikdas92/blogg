# Blogg platfrom

### How to run

Using yarn

```
yarn install
yarn run start
```
Using npm

```
npm install
npm run start
```
The server will be running on port 3000 by default. Visit [http://localhost:3000](http://localhost:3000)

### To run tests

Run this command
```
yarn run test
```

or for npm

```
npm run test
```