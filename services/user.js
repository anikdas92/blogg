const { User, UserJsonSchema } = require("../models/user");
const bcrype = require("bcrypt-nodejs");
const Joi = require("joi");

const signInPayloadSchema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required()
})

/**
 * 
 * @param {String} plainTextPassword 
 * @returns {String} bcrypt hex digest
 */
function generatePasswordHash(plainTextPassword) {
    return bcrype.hashSync(plainTextPassword);
}


/**
 * Checks plain text password againt password hash
 * Stored in database
 * 
 * @param {String} plainTextPassword 
 * @param {String} hash 
 * 
 * @returns {Boolean} true of valid; false if invalid
 */
function checkPassword(plainTextPassword, hash) {
    return bcrype.compareSync(plainTextPassword, hash);
}


/**
 * 
 * @param {Object} payload - json payload from client
 * @returns {Boolean} true of valid; false if invalid
 */
function validateUserSchema(payload) {
    return Joi.validate(payload, UserJsonSchema);
}

/**
 * This function creates an User object with
 * the the payload received from client
 * 
 * @param {Object} payload - json payload from client
 * @returns {User} - an instance of the created User object
 */
async function createUser(payload) {
    const { error: validationError, value: userJson } = validateUserSchema(payload);

    if(validationError){
        // TODO: Log
        throw validationError;
    }else{
        userJson.password = generatePasswordHash(userJson.password);
    }

    return User.create(userJson);

}

/**
 * function to authenticate an user
 * @param {Object} payload - Json payload from client
 */
async function signIn(payload) {
    const { error, value: validatedPayload } = Joi.validate(payload, signInPayloadSchema);

    if (error) {
        //TODO: log error
        throw error;
    }

    let user;
    try {
        user = await User.findOne({
            where: {
                email: validatedPayload.email
            }
        })
    } catch (error) {
        // TODO: log
        throw Error("An error occurred");
    }

    if(user === null) {
        throw Error("Wrong credentials");
    }else{
        const correctPass = checkPassword(validatedPayload.password, user.password);
        if(!correctPass) {
            throw Error("Wrong credentials");
        }
    }
    return {
        id: user.id,
        name: user.name,
        email: user.email
    }
}

module.exports = {
    createUser,
    signIn
}