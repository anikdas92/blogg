const { Comment, CommentJsonSchema } = require("../models/comment");
const { User } = require("../models/user");
const { Post } = require("../models/post");
const Joi = require("joi");


/**
 * 
 * @param {Object} payload - Json payload from client for validation
 * @returns {Boolean} true of valid false otherwise 
 */
function validateCommentSchema(payload) {
    return Joi.validate(payload, CommentJsonSchema);
}


/**
 * This function adds an comment for a user
 * 
 * @param {Object} payload - Json payload from client
 * @param {Number} postId - id of the post
 * @param {Object} user - user Object from session
 * 
 * @returns {Comment} Instance of the created comment
 */
async function addComment(payload, postId, user) {
    const { error: validationError, value: commentJson } = validateCommentSchema(payload);

    if(validationError){
        // TODO: Log
        throw validationError;
    }

    return Comment.create({
        content: commentJson.content,
        userId: user.id,
        postId: postId
    })

}

module.exports = {
    addComment
}