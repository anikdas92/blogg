const { Post, PostJsonSchema } = require("../models/post");
const { Comment } = require("../models/comment");
const { User } = require("../models/user");
const Joi = require("joi");


/**
 * 
 * @param {*} payload - Json payload for validation
 * @returns {Boolean} true of valid false otherwise 
 */
function validatePostSchema(payload) {
    return Joi.validate(payload, PostJsonSchema);
}


/**
 * This function creates a post for a user
 * 
 * @param {Object} payload - Json payload from client
 * @param {Object} user - User object from session
 * 
 * @returns {Post} Created post instance
 */
async function createPost(payload, user) {
    const { error: validationError, value: postJson } = validatePostSchema(payload);

    if(validationError){
        // TODO: Log
        throw validationError;
    }

    return Post.create({
        title: postJson.title,
        content: postJson.content,
        userId: user.id
    })

}


/**
 * This function gets a post by it's post id
 * @param {Number} postId 
 * @returns {Post} an post instance with the post id, null if not found
 */
async function getPost(postId) {

    let post;
    try {
        post = await Post.findOne({
            where: {
                id: postId
            },
            include: [ { 
                model: Comment,
                include: [ {
                    model: User,
                    attributes: ["id", "name"]
                } ]
             }, User ]
        })
    } catch (error) {
        throw new Error("Could not fetch post");
    }

    return post;

}

/**
 * This function returns all the posts
 * @returns {Array} an array of Posts
 */
async function getAllPosts() {

    let posts = [];
    try {
        posts = await Post.findAll({
            attributes: [ "id", "title", "createdAt" ],
            order: [ ["createdAt", "DESC"] ],
            include: [ User ],
        })
    } catch (error) {
        throw new Error("Could not fetch post");
    }

    return posts || [];

}


/**
 * This function gets all the posts by a user
 * @param {Object} user - user from request session
 * @returns {Array} Array of Post objects
 */
async function getPostsByUser(user) {

    let posts = [];
    try {
        posts = await Post.findAll({
            where: {
                userId: user.id
            },
            attributes: [ "id", "title", "createdAt" ],
            order: [ ["createdAt", "DESC"] ]
        })
    } catch (error) {
        throw new Error("Could not fetch post");
    }

    return posts || [];

}

module.exports = {
    createPost,
    getPost,
    getAllPosts,
    getPostsByUser
}