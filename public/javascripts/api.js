function apiCall(method, path, payload) {

    return new Promise((resolve, reject) => {
        var options = {
            method: method,
            url: path
        }
    
        payload && method === "GET" ? options.params = payload : null;
        payload && method === "POST" ? options.data = payload : null;
    
        axios(options)
            .then(function (response) {
                if(response.status === 403) {
                    window.location = '/signin'
                }else{
                    resolve(response.data);
                }
            }, function (error) {
                alert(error);
                console.error(error);
                reject(error);
            });
    });
}


function signin(payload) {
    return apiCall("POST", "/users/signin", payload);    
}

function signup(payload) {
    return apiCall("POST", "/users/signup", payload);    
}

function createPost(payload) {
    return apiCall("POST", "/posts/create", payload);    
}

function addComment(payload) {
    return apiCall("POST", window.location.pathname + "/add_comment", payload);    
}

