var commentForm = document.getElementById("commentForm");
commentForm ? commentForm.onsubmit = function (evt) {
    evt.preventDefault();

    var payload = {
        content: document.getElementById("commentContent").value,
    };

    document.getElementById("submitComment").disabled = true;
    
    addComment(payload).then(response => {
        
        document.getElementById("submitComment").disabled = false;
        
        if(response.success){

            document.getElementById("commentContent").value = ""
            addCommentBlock(response.data);
        }else{
            alert(response.message);
        }
    })
} : null;

function addCommentBlock(data) {

    var comment = data.comment;
    var user = data.user;

    var dummyCommentNode = document.getElementById("dummyCommentSection");
    var commentNode = dummyCommentNode.cloneNode(true);
    commentNode.querySelector(".comment-heading").innerText = user.name;
    commentNode.querySelector(".comment-body").innerText = comment.content;

    commentNode.style.display = "";

    document.getElementById("jsCommentsContainer").appendChild(commentNode);

}