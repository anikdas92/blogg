document.getElementById("newPostForm").onsubmit = function (evt) {
    evt.preventDefault();

    var payload = {
        title: document.getElementById("title").value,
        content: document.getElementById("postContent").value
    };

    createPost(payload).then(response => {
        if(response.success){
            window.location = "/posts/" + response.data.id;
        }else{
            alert(response.message);
        }
    })
}