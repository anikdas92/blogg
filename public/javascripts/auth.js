var signInForm = document.getElementById("signin");
signInForm ? signInForm.onsubmit = function (evt) {
    evt.preventDefault();

    var payload = {
        email: document.getElementById("email").value,
        password: document.getElementById("password").value
    };

    signin(payload).then(data => {
        if(data.success){
            window.location = "/";
        }else{
            alert(data.message);
        }
    })
} : null;


var signUpForm = document.getElementById("signup");

signUpForm ? document.getElementById("signup").onsubmit = function (evt) {
    evt.preventDefault();

    var password = document.getElementById("password").value;
    var confirmPass = document.getElementById("confPassword").value;

    if(password !==confirmPass) {
        alert("Passwords do not match");
        return false;
    }

    var payload = {
        name: document.getElementById("name").value,
        email: document.getElementById("email").value,
        password: password
    };

    signup(payload).then(data => {
        if(data.success){
            window.location = "/";
        }else{
            alert(data.message);
        }
    })
} : null;