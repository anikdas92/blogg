const { User } = require("./user");
const { Post } = require("./post");
const { Comment } = require("./comment");

Post.belongsTo(User);
Comment.belongsTo(User);
Comment.belongsTo(Post);
User.hasMany(Post);
Post.hasMany(Comment);
User.hasMany(Comment);



User.sync()
    .then(() => {
        // TODO: log table created
    })
    .error(err => {
        // TODO: Log
        throw err;
    })

Post.sync()
    .then(() => {
        // TODO: log table created
    })
    .error(err => {
        // TODO: Log
        throw err;
    })

Comment.sync()
    .then(() => {
        // TODO: log table created
    })
    .error(err => {
        // TODO: Log
        throw err;
    })