const Sequelize = require("sequelize");
const Joi = require("joi");

const { User } = require("./user");
const { Comment } = require("./comment");

const sequelize = require("../db/connection");

const Post = sequelize.define('post', {
    title: {
        type: Sequelize.STRING
    },
    content: {
        type: Sequelize.TEXT
    }
});

PostJsonSchema = Joi.object().keys({
    title: Joi.string().required(),
    content: Joi.string().required()
});

module.exports = {
    Post,
    PostJsonSchema
}