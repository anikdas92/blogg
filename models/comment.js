const Sequelize = require("sequelize");
const Joi = require("joi");
const sequelize = require("../db/connection");

const Comment = sequelize.define('comment', {
    content: {
        type: Sequelize.TEXT
    }
});

CommentJsonSchema = Joi.object().keys({
    content: Joi.string().required()
});

module.exports = {
    Comment,
    CommentJsonSchema
}