const Sequelize = require("sequelize");
const Joi = require("joi");

const sequelize = require("../db/connection");

const User = sequelize.define('user', {
    name: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING,
        unique: true
    },
    password: {
        type: Sequelize.STRING
    }
});

UserJsonSchema = Joi.object().keys({
    name: Joi.string().required(),
    password: Joi.string().required(),
    email: Joi.string().email().required()
});

module.exports = {
    User,
    UserJsonSchema
}