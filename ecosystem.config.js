module.exports = {
  apps : [

    {
      name      : 'blogg',
      script    : 'bin/www',
      env: {
        PORT: 3000
      },
      env_production : {
        NODE_ENV: 'production',
        PORT: 3002
      }
    }
  ]
};
